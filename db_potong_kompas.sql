-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.24-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for laravel_baseapp
DROP DATABASE IF EXISTS `laravel_baseapp`;
CREATE DATABASE IF NOT EXISTS `laravel_baseapp` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `laravel_baseapp`;

-- Dumping structure for table laravel_baseapp.failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel_baseapp.failed_jobs: ~0 rows (approximately)

-- Dumping structure for table laravel_baseapp.menus
DROP TABLE IF EXISTS `menus`;
CREATE TABLE IF NOT EXISTS `menus` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `parrent_menu_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menus_parrent_menu_id_index` (`parrent_menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel_baseapp.menus: ~16 rows (approximately)
INSERT INTO `menus` (`id`, `parrent_menu_id`, `name`, `link`, `urutan`, `created_at`, `updated_at`) VALUES
	(1, 0, 'Dashboard', 'dashboard', 1, NULL, NULL),
	(2, 0, 'Menu Management', 'menu', 99, NULL, '2023-01-05 01:36:33'),
	(3, 0, 'Role Management', 'role', 99, NULL, '2023-01-05 01:37:14'),
	(4, 0, 'User Management', 'user', 99, NULL, '2023-01-05 01:38:01'),
	(5, 0, 'Daftar Produk', 'product', 2, '2023-01-05 00:20:08', '2023-01-05 01:36:17'),
	(6, 0, 'Keranjang Belanja', 'shopping_cart', 3, '2023-01-05 01:23:23', '2023-01-10 22:44:10'),
	(7, 0, 'Riwayat Pembelian', 'shopping_history', 4, '2023-01-05 01:24:06', '2023-01-05 01:37:35'),
	(8, 0, 'Riwayat Penyewaan', 'riwayat_penyewaan', 5, '2023-01-05 01:24:36', '2023-01-05 01:37:51'),
	(9, 0, 'Verifikasi Transaksi Pembelian', 'verifikasi_transaksi_pembelian', 6, '2023-01-05 01:24:58', '2023-01-10 22:55:36'),
	(10, 0, 'Verifikasi Transaksi Penyewaan', 'verifikasi_transaksi_penyewaan', 7, '2023-01-05 01:25:18', '2023-01-10 22:55:24'),
	(11, 0, 'Home', 'home', 0, '2023-01-05 01:36:02', '2023-01-05 01:36:02'),
	(12, 0, 'Kelola Produk', 'kelola_produk', 8, '2023-01-05 01:52:30', '2023-01-05 01:52:38'),
	(13, 0, 'Kelola Kurir Pengiriman', 'kelola_kurir_pengiriman', 9, '2023-01-10 22:56:46', '2023-01-10 22:56:46'),
	(14, 0, 'Laporan Penjualan', 'laporan_penjualan', 10, '2023-01-10 22:57:24', '2023-01-10 22:57:24'),
	(15, 0, 'Laporan Penyewaan', 'laporan_penyewaan', 11, '2023-01-10 22:57:46', '2023-01-10 22:57:46'),
	(16, 0, 'Kelola Akun Bank', 'kelola_akun_bank', 12, '2023-01-10 23:01:16', '2023-01-10 23:01:16');

-- Dumping structure for table laravel_baseapp.migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel_baseapp.migrations: ~9 rows (approximately)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(5, '2022_02_07_135603_create_roles_table', 1),
	(6, '2022_02_07_135829_add_role_code_to_user', 1),
	(7, '2022_02_10_141322_create_menus_table', 1),
	(8, '2022_02_22_104422_create_role_accesses_table', 1),
	(9, '2022_02_23_001534_add_access_to_role_accesses', 1);

-- Dumping structure for table laravel_baseapp.password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel_baseapp.password_resets: ~0 rows (approximately)

-- Dumping structure for table laravel_baseapp.personal_access_tokens
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel_baseapp.personal_access_tokens: ~0 rows (approximately)

-- Dumping structure for table laravel_baseapp.roles
DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `roles_role_code_index` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel_baseapp.roles: ~4 rows (approximately)
INSERT INTO `roles` (`id`, `role_code`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'superadmin', 'Super Admin', '2022-07-24 19:39:42', '2022-07-24 19:39:42'),
	(2, 'customer', 'Customer / Pelanggan', '2023-01-05 00:21:29', '2023-01-05 01:26:58'),
	(3, 'admin', 'Administrator', '2023-01-05 01:25:56', '2023-01-10 22:58:17'),
	(4, 'admin_finance', 'Admin Finance', '2023-01-10 22:58:38', '2023-01-10 22:58:38');

-- Dumping structure for table laravel_baseapp.role_accesses
DROP TABLE IF EXISTS `role_accesses`;
CREATE TABLE IF NOT EXISTS `role_accesses` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `access` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `create` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `update` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `delete` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_accesses_role_code_index` (`role_code`),
  KEY `role_accesses_menu_id_index` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel_baseapp.role_accesses: ~17 rows (approximately)
INSERT INTO `role_accesses` (`id`, `role_code`, `menu_id`, `access`, `view`, `create`, `update`, `delete`, `created_at`, `updated_at`) VALUES
	(2, 'superadmin', 2, NULL, 'Y', 'Y', 'Y', 'Y', NULL, NULL),
	(3, 'superadmin', 3, NULL, 'Y', 'Y', 'Y', 'Y', NULL, NULL),
	(4, 'superadmin', 4, NULL, 'Y', 'Y', 'Y', 'Y', NULL, NULL),
	(6, 'customer', 5, NULL, NULL, 'Y', 'Y', 'Y', '2023-01-05 00:22:15', '2023-01-05 00:22:20'),
	(8, 'customer', 6, NULL, NULL, NULL, NULL, NULL, '2023-01-05 01:25:28', '2023-01-05 01:25:28'),
	(9, 'customer', 7, NULL, NULL, 'Y', 'Y', 'Y', '2023-01-05 01:25:32', '2023-01-05 01:25:38'),
	(10, 'customer', 8, NULL, NULL, 'Y', 'Y', 'Y', '2023-01-05 01:25:33', '2023-01-05 01:25:40'),
	(14, 'admin', 4, NULL, NULL, NULL, NULL, NULL, '2023-01-05 01:26:13', '2023-01-05 01:26:13'),
	(15, 'superadmin', 11, NULL, NULL, NULL, NULL, NULL, '2023-01-05 01:38:47', '2023-01-05 01:38:47'),
	(17, 'admin', 11, NULL, NULL, NULL, NULL, NULL, '2023-01-05 01:38:55', '2023-01-05 01:38:55'),
	(18, 'customer', 11, NULL, NULL, NULL, NULL, NULL, '2023-01-05 01:39:03', '2023-01-05 01:39:03'),
	(20, 'admin', 13, NULL, NULL, NULL, NULL, NULL, '2023-01-10 23:00:28', '2023-01-10 23:00:28'),
	(21, 'admin', 12, NULL, NULL, NULL, NULL, NULL, '2023-01-10 23:00:29', '2023-01-10 23:00:29'),
	(22, 'admin', 16, NULL, NULL, NULL, NULL, NULL, '2023-01-10 23:01:29', '2023-01-10 23:01:29'),
	(23, 'admin_finance', 9, NULL, NULL, NULL, NULL, NULL, '2023-01-10 23:02:36', '2023-01-10 23:02:36'),
	(24, 'admin_finance', 10, NULL, NULL, NULL, NULL, NULL, '2023-01-10 23:02:37', '2023-01-10 23:02:37'),
	(25, 'admin_finance', 14, NULL, NULL, NULL, NULL, NULL, '2023-01-10 23:02:44', '2023-01-10 23:02:44'),
	(26, 'admin_finance', 15, NULL, NULL, NULL, NULL, NULL, '2023-01-10 23:02:45', '2023-01-10 23:02:45'),
	(27, 'admin_finance', 11, NULL, NULL, NULL, NULL, NULL, '2023-01-10 23:02:46', '2023-01-10 23:02:46');

-- Dumping structure for table laravel_baseapp.users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_code_index` (`role_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel_baseapp.users: ~5 rows (approximately)
INSERT INTO `users` (`id`, `role_code`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'superadmin', 'Super Admin', 'superadmin@mail.com', NULL, '$2y$10$gG5Hr7erTL.zmwoMalPLJ..0AOEgySCRzQiGHE2ju2jsm.rm6dMkO', NULL, '2022-07-24 19:39:56', '2022-07-24 19:39:56'),
	(2, 'superadmin', 'Testing', 'lemanfoetra@gmail.com', NULL, '$2y$10$A.qL8jQOYoDBO5wjfwT21ezHrFUl1iN5n8FOiP9VNonf/QuGsNKIC', NULL, '2022-07-24 19:49:00', '2022-07-24 19:49:00'),
	(3, 'customer', 'Ardyn Sulaeman', 'ardyn.sulaeman@gmail.com', NULL, '$2y$10$gZ.xKIhkrckNKvEc/vmZeOpoFK9MGElS0iJ3lk7TRCdbdY3iDgeCy', NULL, '2023-01-05 00:22:05', '2023-01-05 00:22:05'),
	(4, 'customer', 'Customer', 'customer@mail.com', NULL, '$2y$10$SwQjvTlgOKT.BArOrZwsUedfX35nV0ZKOgn5GB.Dq7ldovbLHF32u', NULL, '2023-01-05 01:27:40', '2023-01-10 23:03:19'),
	(5, 'admin', 'Administrator', 'administrator@mail.com', NULL, '$2y$10$Jqh4famP.394HczIvrw8x.z2M0o7yLTp9eadldocuKVPbb4bz33wG', NULL, '2023-01-05 01:28:07', '2023-01-10 23:03:47'),
	(6, 'admin_finance', 'Admin Finance', 'finance@mail.com', NULL, '$2y$10$HLkn/sQlxbvPWIXAQ/Gxs.d4bAiruLfgniINg.XEW4aK5hVsl8BIm', NULL, '2023-01-10 23:04:29', '2023-01-10 23:04:29');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
