@extends('template.backend')
@section('title', 'Dashboard')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div>
            <div>
                <h4>{{ $title ?? '-' }}</h4>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-style1">
                    @if(isset($breadcrumb))
                    @foreach($breadcrumb as $i => $br)
                    @if(($i + 1) == count($breadcrumb))
                    <li class="breadcrumb-item active">{{ $br['title'] ?? '-' }}</li>
                    @else
                    <li class="breadcrumb-item">
                        <a href="{{ $br['link'] ?? '#' }}">{{ $br['title'] ?? '-' }}</a>
                    </li>
                    @endif
                    @endforeach
                    @endif
                </ol>
            </nav>
        </div>
    </div>
    <!-- /breadcrumb -->

    <div class="row">
        <div class="col-xl-3  col-sm-6 col-6">
            <div class="card">
                <div class="card-body h-100">
                    <div class="pro-img-box">
                        <img class="w-100" src="{{ url('templates-backend/img/Borneo.4.jpg') }}" alt="product-image">
                        <a href="#" class="adtocart"> <i class="las la-shopping-cart "></i>
                        </a>
                    </div>
                    <div class="text-center pt-3">
                        <h3 class="h6 mb-2 mt-4 font-weight-bold text-uppercase">Tenda Borneo 4</h3>
                        <h4 class="h5 mb-0 mt-2 text-center text-primary">Beli Rp 500.000</h4>
                        <h4 class="h6 mb-0 mt-2 text-center text-danger">Sewa Rp 50.000 / Hari</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3  col-sm-6 col-6">
            <div class="card">
                <div class="card-body h-100">
                    <div class="pro-img-box">
                        <img class="w-100" src="{{ url('templates-backend/img/Borneo.4.jpg') }}" alt="product-image">
                        <a href="#" class="adtocart"> <i class="las la-shopping-cart "></i>
                        </a>
                    </div>
                    <div class="text-center pt-3">
                        <h3 class="h6 mb-2 mt-4 font-weight-bold text-uppercase">Tenda Borneo 4</h3>
                        <h4 class="h5 mb-0 mt-2 text-center text-primary">Beli Rp 500.000</h4>
                        <h4 class="h6 mb-0 mt-2 text-center text-danger">Sewa Rp 50.000 / Hari</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3  col-sm-6 col-6">
            <div class="card">
                <div class="card-body h-100">
                    <div class="pro-img-box">
                        <img class="w-100" src="{{ url('templates-backend/img/Borneo.4.jpg') }}" alt="product-image">
                        <a href="#" class="adtocart"> <i class="las la-shopping-cart "></i>
                        </a>
                    </div>
                    <div class="text-center pt-3">
                        <h3 class="h6 mb-2 mt-4 font-weight-bold text-uppercase">Tenda Borneo 4</h3>
                        <h4 class="h5 mb-0 mt-2 text-center text-primary">Beli Rp 500.000</h4>
                        <h4 class="h6 mb-0 mt-2 text-center text-danger">Sewa Rp 50.000 / Hari</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3  col-sm-6 col-6">
            <div class="card">
                <div class="card-body h-100">
                    <div class="pro-img-box">
                        <img class="w-100" src="{{ url('templates-backend/img/Borneo.4.jpg') }}" alt="product-image">
                        <a href="#" class="adtocart"> <i class="las la-shopping-cart "></i>
                        </a>
                    </div>
                    <div class="text-center pt-3">
                        <h3 class="h6 mb-2 mt-4 font-weight-bold text-uppercase">Tenda Borneo 4</h3>
                        <h4 class="h5 mb-0 mt-2 text-center text-primary">Beli Rp 500.000</h4>
                        <h4 class="h6 mb-0 mt-2 text-center text-danger">Sewa Rp 50.000 / Hari</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3  col-sm-6 col-6">
            <div class="card">
                <div class="card-body h-100">
                    <div class="pro-img-box">
                        <img class="w-100" src="{{ url('templates-backend/img/Borneo.4.jpg') }}" alt="product-image">
                        <a href="#" class="adtocart"> <i class="las la-shopping-cart "></i>
                        </a>
                    </div>
                    <div class="text-center pt-3">
                        <h3 class="h6 mb-2 mt-4 font-weight-bold text-uppercase">Tenda Borneo 4</h3>
                        <h4 class="h5 mb-0 mt-2 text-center text-primary">Beli Rp 500.000</h4>
                        <h4 class="h6 mb-0 mt-2 text-center text-danger">Sewa Rp 50.000 / Hari</h4>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-xl-3  col-sm-6 col-6">
            <div class="card">
                <div class="card-body h-100">
                    <div class="pro-img-box">
                        <img class="w-100" src="{{ url('templates-backend/img/Borneo.4.jpg') }}" alt="product-image">
                        <a href="#" class="adtocart"> <i class="las la-shopping-cart "></i>
                        </a>
                    </div>
                    <div class="text-center pt-3">
                        <h3 class="h6 mb-2 mt-4 font-weight-bold text-uppercase">Tenda Borneo 4</h3>
                        <h4 class="h5 mb-0 mt-2 text-center text-primary">Beli Rp 500.000</h4>
                        <h4 class="h6 mb-0 mt-2 text-center text-danger">Sewa Rp 50.000 / Hari</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection