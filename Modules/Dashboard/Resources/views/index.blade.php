@extends('template.backend')
@section('title', 'Dashboard')

@section('content')
<!-- container opened -->
<div class="container">

    <!-- breadcrumb -->
    <div class="breadcrumb-header justify-content-between">
        <div class="left-content">
            <div>
                <h2 class="main-content-title tx-24 mg-b-1 mg-b-lg-1">Selamat datang di aplikasi potong kompas</h2>
            </div>
        </div>
    </div>
</div>
@endsection